# Quest Notifier bot
Телеграм бот, позволяющий следить за появлением локальных заданий в World Of Warcraft и оповещать об их появлении.

### Использование
```
go run cmd/main.go --config=<path_to_config_file>
```
или c использованием докера
```
docker container run -v $(pwd)/configs/:/go/questnotifierbot/configs <image_name>
```

### Настройки
По умолчанию считается, что путь к главному файлу настроек это `configs/config.yml` (пример `configs/config_example.yml`). 

Кроме него, необходим файл со списком квестов, которые будут отслеживаться (пример `configs/wowhead_client_settings.yml`). В него нужно передать ссылки на карты квестов, которые необходимо отслеживать, а также фильтры, аналогичные тем, что можно выставить в https://www.wowhead.com/quests