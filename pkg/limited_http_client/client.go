package limited_http_client

import (
	"context"
	"net/http"
	"time"

	"golang.org/x/time/rate"
)

// Should be created via NewLimitedClient
type LimitedHttpClient struct {
	client  *http.Client
	limiter *rate.Limiter
}

func NewLimitedClient(limit float64, burst int, timeout time.Duration) *LimitedHttpClient {
	return &LimitedHttpClient{
		client: &http.Client{
			Timeout: timeout,
		},
		limiter: rate.NewLimiter(rate.Limit(limit), burst),
	}
}

func (c *LimitedHttpClient) Get(url string) (*http.Response, error) {
	err := c.limiter.Wait(context.Background())
	if err != nil {
		return nil, err
	}

	return c.client.Get(url)
}
