package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"

	"quest_notifier_bot/internal/app"
	"quest_notifier_bot/internal/storage/postgresql"
	"quest_notifier_bot/internal/telegram"
	wowhead "quest_notifier_bot/internal/wowhead/client"

	"quest_notifier_bot/internal/coreserver"
)

var (
	log = logrus.WithField("service", "main")
)

func init() {
	logrus.StandardLogger().SetFormatter(
		&logrus.TextFormatter{ForceColors: true},
	)
}

type Config struct {
	Storage  postgresql.Config
	Wowhead  wowhead.Config
	Telegram telegram.Config
}

func main() {
	var configFileName string
	flag.StringVar(&configFileName, "config", "configs/config.yml", "path to yaml config file")
	flag.Parse()

	config, err := app.NewConfigFromYamlFile(configFileName)
	if err != nil {
		log.WithFields(logrus.Fields{
			"result": "error",
			"data":   err,
		}).Errorf("failed to create config")
		return
	}

	storage, err := postgresql.NewStorageFromConfig(config.Storage)
	if err != nil {
		log.WithFields(logrus.Fields{
			"result": "error",
			"data":   err,
			"config": config.Storage,
		}).Errorf("failed to create storage from config")
		return
	}
	defer storage.Close()

	coreServerImpl, err := coreserver.NewServer(storage)
	if err != nil {
		log.WithFields(logrus.Fields{
			"result": "error",
			"data":   err,
		}).Errorf("failed to create server")
		return
	}

	var server coreserver.CoreServer = coreserver.NewLoggingServer(coreServerImpl, logrus.WithField("service", "server"))

	wowhead, err := wowhead.NewWowheadClientFromConfig(storage.Quest(), config.Wowhead)
	if err != nil {
		log.WithFields(logrus.Fields{
			"result": "error",
			"data":   err,
			"config": config.Wowhead,
		}).Errorf("failed to create wowhead client")
		return
	}
	wowhead.SetNotifiable(server)

	telegram, err := telegram.NewServer(server, config.Telegram)
	if err != nil {
		log.WithFields(logrus.Fields{
			"result": "error",
			"data":   err,
			"config": config.Telegram,
		}).Errorf("failed to create telegram client")
		return
	}
	server.SetUserNotifiable(telegram)

	log.Info("ready to start")
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	go wowhead.Run()
	go telegram.Run()

	log.Infof("recived %s, terminating...", <-exit)
	server.Shutdown()
	telegram.Shutdown()
	wowhead.Shutdown()
	log.Info("terminated")
}
