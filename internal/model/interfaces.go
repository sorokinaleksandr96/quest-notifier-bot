package model

import "net/http"

type HTTPGetter interface {
	Get(string) (*http.Response, error)
}

type QuestNotifiable interface {
	// Should be non blocking
	GetNotified(quests []Quest) chan error
}
