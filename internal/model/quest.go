package model

import (
	"encoding/json"
	"strconv"
	"time"
)

type QuestID int

type Quest struct {
	ID     QuestID
	Name   string
	Ending time.Time
}

func (q *Quest) UnmarshalJSON(data []byte) error {
	type tmp struct {
		ID     int
		Name   string
		Ending int64
	}

	t := tmp{}
	err := json.Unmarshal(data, &t)
	if err != nil {
		return err
	}

	q.ID = QuestID(t.ID)
	q.Name = t.Name
	q.Ending = time.Unix(t.Ending/1000, 0)
	return nil
}

func getZeroTimestamp() time.Time {
	res, err := time.Parse(time.RFC3339, "0001-01-01T00:00:00Z")
	if err != nil {
		panic(err)
	}

	return res
}

var zeroTimestamp = getZeroTimestamp()

func (q Quest) MarshalJSON() ([]byte, error) {
	if q.Ending == zeroTimestamp {
		return []byte(`{` + `"id":` + strconv.Itoa(int(q.ID)) + `}`), nil
	}
	return []byte(`{` + `"id":` + strconv.Itoa(int(q.ID)) + `,"time":` + strconv.FormatInt(q.Ending.Unix()*1000, 10) + `}`), nil
}
