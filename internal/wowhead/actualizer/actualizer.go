package actualizer

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	"quest_notifier_bot/internal/model"
	"quest_notifier_bot/internal/storage"
)

var (
	wqListRegexp = regexp.MustCompile(`new Listview\(.*data:(.*).*}\)`)

	log = logrus.WithField("service", "wowhead_actualizer")
)

// Should be created via NewQuestActualizer()
type QuestActualizer struct {
	getter model.HTTPGetter

	storage storage.QuestRepository

	slGetter SLGetter

	updateInterval time.Duration

	shutdownCh   chan struct{}
	shutdownedCh chan struct{}
}

func NewQuestActualizer(cl model.HTTPGetter, s storage.QuestRepository, slg SLGetter, updInt time.Duration) (*QuestActualizer, error) {
	if cl == nil {
		return nil, fmt.Errorf("http getter not provided")
	}

	if s == nil {
		return nil, fmt.Errorf("storage not provided")
	}

	if slg == nil {
		return nil, fmt.Errorf("search list getter not provided")
	}

	if updInt == 0 {
		updInt = time.Hour * 7 * 24
	}

	return &QuestActualizer{
		getter:         cl,
		storage:        s,
		slGetter:       slg,
		updateInterval: updInt,
		shutdownCh:     make(chan struct{}),
		shutdownedCh:   make(chan struct{}),
	}, nil
}

func (a *QuestActualizer) MakeUpdate() ([]model.Quest, error) {
	sl, err := a.slGetter()
	if err != nil {
		return nil, err
	}

	urls, err := sl.GenerateURLs()
	if err != nil {
		return nil, err
	}

	eg, _ := errgroup.WithContext(context.Background())
	results := make(chan []model.Quest, len(urls))
	for _, url := range urls {

		u := url
		eg.Go(func() error {
			return a.processUrl(u, results)
		})
	}

	err = eg.Wait()
	if err != nil {
		return nil, err
	}
	close(results)

	updates := []model.Quest{}
	for result := range results {
		updates = append(updates, result...)
	}

	err = a.storage.Update(updates)
	if err != nil {
		log.WithError(err).Error("failed to update")
	} else {
		log.WithField("num_added", len(updates)).Info("successfully updated")
	}
	return updates, err
}

func (a *QuestActualizer) Run() {
	timer := time.NewTimer(a.updateInterval)
	defer timer.Stop()
	defer close(a.shutdownedCh)

	for {
		select {
		case <-a.shutdownCh:
			return
		case <-timer.C:
			// Logging is already in MakeUpdate
			_, _ = a.MakeUpdate()

			timer.Reset(a.updateInterval)
		}
	}
}

func (a *QuestActualizer) Shutdown() {
	close(a.shutdownCh)
	<-a.shutdownedCh
	log.Info("terminated")
}

func (a *QuestActualizer) processUrl(url string, o chan<- []model.Quest) error {
	resp, err := a.getter.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var result []model.Quest
	tmp := wqListRegexp.FindSubmatch(data)
	if len(tmp) < 2 {
		return nil
	}

	err = json.Unmarshal(tmp[1], &result)
	if err != nil {
		return err
	}

	o <- result
	return nil
}
