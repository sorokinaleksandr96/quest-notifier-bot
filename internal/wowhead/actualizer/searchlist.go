package actualizer

import (
	"fmt"
	"os"
	"strings"

	"gopkg.in/yaml.v3"
)

type SLGetter func() (SearchList, error)

func SLGetterFromYaml(filepath string) SLGetter {
	return func() (SearchList, error) {
		content, err := os.ReadFile(filepath)
		if err != nil {
			return SearchList{}, err
		}

		var t map[string][]string
		err = yaml.Unmarshal(content, &t)
		if err != nil {
			return SearchList{}, err
		}

		if len(t["QuestTypes"]) == 0 || len(t["Patches"]) == 0 {
			return SearchList{}, fmt.Errorf("quest types or patches are missed in search list")
		}

		return SearchList{
			QuestTypes: t["QuestTypes"],
			Patches:    t["Patches"],
			Manual:     t["Manual"],
		}, nil
	}
}

type SearchList struct {
	QuestTypes []string
	Patches    []string

	// model.QuestIDs but as strings
	Manual []string
}

const (
	questsUrl = "https://www.wowhead.com/quests"
)

func (sl SearchList) GenerateURLs() ([]string, error) {
	urls := []string{}

	if len(sl.QuestTypes) < 1 {
		return urls, fmt.Errorf("quest types not provided")
	}
	qtstr := strings.Join(sl.QuestTypes, ":")

	for i := range sl.Patches {
		var (
			query string
			err   error
		)

		if i != len(sl.Patches)-1 {
			query, err = generateTypeAndPatchQuery(qtstr, sl.Patches[i], sl.Patches[i+1])
		} else {
			query, err = generateTypeAndPatchQuery(qtstr, sl.Patches[i])
		}

		if err != nil {
			return urls, err
		} else {
			urls = append(urls, questsUrl+query)
		}
	}

	if len(sl.Manual) > 0 {
		urls = append(urls, questsUrl+GenerateManuallIDQuery(sl.Manual))
	}

	return urls, nil
}

const (
	filter_code = "8"
	ge_code     = "2"
	eq_code     = "3"
	l_code      = "5"
)

// generateTypeAndPatchQuery produces query for quest searching request.
// qts should contain colon separated quest type ids, and
// ps should contain 1 or 2 patch numbers to search wqs added
// in [p_0, p_1) or in p_0
func generateTypeAndPatchQuery(qts string, ps ...string) (string, error) {
	if len(ps) < 1 || len(ps) > 2 {
		return "", fmt.Errorf("ps should contain 1 or 2 patch numbers")
	}

	var patchFilter string
	if len(ps) == 2 {
		patchFilter = fmt.Sprintf("%s:%s;%s:%s;%s:%s",
			filter_code, filter_code, ge_code, l_code, ps[0], ps[1],
		)
	} else {
		patchFilter = fmt.Sprintf("%s;%s;%s", filter_code, eq_code, ps[0])
	}

	return "/type:" + qts + "?filter=" + patchFilter, nil
}

func GenerateManuallIDQuery(ids []string) string {
	filterTypes := strings.Repeat("30:", len(ids))
	eqs := strings.Repeat(eq_code+":", len(ids))

	return "?filter-any=" + filterTypes[:len(filterTypes)-1] + ";" +
		eqs[:len(eqs)-1] + ";" + strings.Join(ids, ":")
}
