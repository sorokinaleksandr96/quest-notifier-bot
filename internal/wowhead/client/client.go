package wowhead

import (
	"fmt"
	"sync"
	"time"

	"quest_notifier_bot/internal/model"
	"quest_notifier_bot/internal/storage"
	"quest_notifier_bot/internal/wowhead/actualizer"
	"quest_notifier_bot/internal/wowhead/tracker"
	"quest_notifier_bot/pkg/limited_http_client"
)

type Config struct {
	RequestTimeout         time.Duration // atlest 5s
	RequestsPerSecondLimit int           // default: 1

	TrackerUpdateInterval    time.Duration // default: 1h
	ActualizerUpdateInterval time.Duration // default: 1week

	PathToQuestsFilterYaml string
}

func (c *Config) Validate() {
	if c.RequestTimeout < 5*time.Second {
		c.RequestTimeout = 5 * time.Second
	}

	if c.RequestsPerSecondLimit == 0 {
		c.RequestsPerSecondLimit = 1
	}

	if c.TrackerUpdateInterval == 0 {
		c.TrackerUpdateInterval = time.Hour
	}

	if c.ActualizerUpdateInterval == 0 {
		c.ActualizerUpdateInterval = 7 * 24 * time.Hour
	}
}

// Should be created with NewWowheadClient*
type WowheadClient struct {
	tracker    *tracker.QuestTracker
	actualizer *actualizer.QuestActualizer

	wg sync.WaitGroup
}

func NewWowheadClientFromConfig(s storage.QuestRepository, c Config) (*WowheadClient, error) {
	c.Validate()

	cl := limited_http_client.NewLimitedClient(float64(c.RequestsPerSecondLimit), 1, c.RequestTimeout)

	tracker, err := tracker.NewQuestTracker(
		cl, s,
		tracker.URLsGetterFromYaml(c.PathToQuestsFilterYaml),
		c.TrackerUpdateInterval,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create QuestTracker: %w", err)
	}

	actualizer, err := actualizer.NewQuestActualizer(
		cl, s,
		actualizer.SLGetterFromYaml(c.PathToQuestsFilterYaml),
		c.ActualizerUpdateInterval,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create QuestActualizer: %w", err)
	}

	_, err = actualizer.MakeUpdate()
	if err != nil {
		return nil, fmt.Errorf("actualizer failed to make update: %w", err)
	}

	return &WowheadClient{
		tracker:    tracker,
		actualizer: actualizer,
	}, nil
}

func NewWowheadClientFromYaml(
	cl model.HTTPGetter, s storage.QuestRepository, filepath string,
	trackerUpdInt time.Duration, actualizerUpdInt time.Duration,
) (*WowheadClient, error) {
	tracker, err := tracker.NewQuestTracker(
		cl, s,
		tracker.URLsGetterFromYaml(filepath),
		trackerUpdInt,
	)
	if err != nil {
		return nil, err
	}

	actualizer, err := actualizer.NewQuestActualizer(
		cl, s,
		actualizer.SLGetterFromYaml(filepath),
		actualizerUpdInt,
	)
	if err != nil {
		return nil, err
	}

	_, err = actualizer.MakeUpdate()
	if err != nil {
		return nil, fmt.Errorf("actualizer failed to make update: %w", err)
	}

	return &WowheadClient{
		tracker:    tracker,
		actualizer: actualizer,
	}, nil
}

func NewWowheadClientFromParts(t *tracker.QuestTracker, a *actualizer.QuestActualizer) (*WowheadClient, error) {
	_, err := a.MakeUpdate()
	if err != nil {
		return nil, fmt.Errorf("actualizer failed to make update: %w", err)
	}

	return &WowheadClient{
		tracker:    t,
		actualizer: a,
	}, nil
}

func (c *WowheadClient) SetNotifiable(n model.QuestNotifiable) {
	c.tracker.SetNotifiable(n)
}

func (c *WowheadClient) Run() {
	c.wg.Add(2)

	go func() {
		defer c.wg.Done()
		c.actualizer.Run()
	}()

	go func() {
		defer c.wg.Done()
		c.tracker.Run()
	}()

	c.wg.Wait()
}

func (c *WowheadClient) Shutdown() {
	c.actualizer.Shutdown()
	c.tracker.Shutdown()
	c.wg.Wait()
}
