package tracker

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type URLsGetter func() ([]string, error)

func URLsGetterFromYaml(filepath string) URLsGetter {
	return func() ([]string, error) {
		content, err := os.ReadFile(filepath)
		if err != nil {
			return nil, err
		}

		var t map[string][]string
		err = yaml.Unmarshal(content, &t)
		if err != nil {
			return nil, err
		}

		if len(t["URLs"]) == 0 {
			return nil, fmt.Errorf("active quests urls are missed")
		}

		return t["URLs"], nil
	}
}
