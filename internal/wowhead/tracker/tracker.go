package tracker

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"regexp"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	"quest_notifier_bot/internal/model"
)

type Namer interface {
	GetNames([]model.QuestID) (map[model.QuestID]string, error)
}
type QuestTracker struct {
	getter model.HTTPGetter
	namer  Namer
	ug     URLsGetter

	seenBefore     map[model.QuestID]struct{}
	updateInterval time.Duration

	// proctected by mu
	qn model.QuestNotifiable
	mu sync.Mutex

	shutdownCh   chan struct{}
	shutdownedCh chan struct{}
}

var (
	wqListRegexp = regexp.MustCompile(`new Listview\((.*)\)`)

	log = logrus.WithField("service", "wowhead_tracker")
)

func NewQuestTracker(g model.HTTPGetter, n Namer, ug URLsGetter, updInt time.Duration) (*QuestTracker, error) {
	if g == nil {
		return nil, fmt.Errorf("http getter not provided")
	}

	if n == nil {
		return nil, fmt.Errorf("namer not provided")
	}

	if ug == nil {
		return nil, fmt.Errorf("urls getter not provided")
	}

	if updInt == 0 {
		updInt = time.Hour
	}

	return &QuestTracker{
		getter: g,
		namer:  n,
		ug:     ug,

		seenBefore:     make(map[model.QuestID]struct{}),
		updateInterval: updInt,

		shutdownCh:   make(chan struct{}),
		shutdownedCh: make(chan struct{}),
	}, nil
}

func (t *QuestTracker) Run() {
	timer := time.NewTimer(0)
	defer timer.Stop()
	defer close(t.shutdownedCh)

	for {
		select {
		case <-t.shutdownCh:
			return
		case <-timer.C:
			acts, err := t.getActiveQuests()
			if err != nil {
				log.WithField("error", err).Error("failed to get active quests")
			} else {
				updates := t.makeUpdate(acts)
				log.WithField("num_updates", len(updates)).Info("successfully update actual quests")
			}

			timer.Reset(t.updateInterval)
		}
	}
}

func (t *QuestTracker) getActiveQuests() ([]model.Quest, error) {
	urls, err := t.ug()
	if err != nil {
		return nil, err
	}

	eg, _ := errgroup.WithContext(context.Background())
	results := make(chan []model.Quest, len(urls))
	for _, url := range urls {
		u := url
		eg.Go(func() error {
			res, err := t.processUrl(u)
			if err != nil {
				return err
			}

			results <- res
			return nil
		})
	}

	err = eg.Wait()
	if err != nil {
		return nil, err
	}
	close(results)

	output := []model.Quest{}
	for resultBatch := range results {
		output = append(output, resultBatch...)
	}

	return output, nil
}

func (t *QuestTracker) makeUpdate(qs []model.Quest) []model.Quest {
	actualNow := map[model.QuestID]struct{}{}
	updates := []model.Quest{}

	for _, q := range qs {
		actualNow[q.ID] = struct{}{}
		if _, seen := t.seenBefore[q.ID]; !seen {
			updates = append(updates, q)
		}
	}

	t.seenBefore = actualNow

	if len(updates) == 0 {
		return updates
	}

	t.mu.Lock()
	if t.qn != nil {
		t.qn.GetNotified(updates)
	}
	t.mu.Unlock()

	return updates
}

func (t *QuestTracker) processUrl(url string) ([]model.Quest, error) {
	resp, err := t.getter.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	quests, err := parseHTMLToQuests(data)
	if err != nil {
		return nil, err
	}

	known, unknown, err := enrichWithNames(quests, t.namer)
	if err != nil {
		return nil, err
	}
	if len(unknown) > 0 {
		log.WithFields(logrus.Fields{
			"url": url,
			"ids": unknown,
		}).Warning("cannot name some quests")
	}

	return known, err
}

func parseHTMLToQuests(data []byte) ([]model.Quest, error) {
	var toParse []byte
	{
		tmp := wqListRegexp.FindSubmatch(data)
		if len(tmp) != 2 {
			return nil, fmt.Errorf("unexpected html format")
		}
		toParse = tmp[1]
	}

	type tmp struct {
		Data []model.Quest
	}
	m := tmp{}
	err := json.Unmarshal(toParse, &m)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal wqs data: %w", err)
	}
	return m.Data, nil
}

func enrichWithNames(qs []model.Quest, n Namer) ([]model.Quest, []model.QuestID, error) {
	ids := make([]model.QuestID, len(qs))
	for i, q := range qs {
		ids[i] = q.ID
	}

	names, err := n.GetNames(ids)
	if err != nil {
		return nil, nil, err
	}

	res := []model.Quest{}
	missed := []model.QuestID{}
	for _, q := range qs {
		if name, exists := names[q.ID]; exists {
			q.Name = name
			res = append(res, q)
		} else {
			missed = append(missed, q.ID)
		}
	}

	return res, missed, nil
}

func (t *QuestTracker) SetNotifiable(n model.QuestNotifiable) {
	t.mu.Lock()
	t.qn = n
	t.mu.Unlock()
}

func (t *QuestTracker) Shutdown() {
	close(t.shutdownCh)
	<-t.shutdownedCh
	log.Info("terminated")
}
