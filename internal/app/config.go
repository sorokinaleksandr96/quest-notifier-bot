package app

import (
	"fmt"
	"os"
	"quest_notifier_bot/internal/storage/postgresql"
	"quest_notifier_bot/internal/telegram"
	wowhead "quest_notifier_bot/internal/wowhead/client"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Storage  postgresql.Config
	Wowhead  wowhead.Config
	Telegram telegram.Config
}

func NewConfigFromYamlFile(filename string) (Config, error) {
	var config Config

	data, err := os.ReadFile(filename)
	if err != nil {
		return config, fmt.Errorf("failed to read file: %w", err)
	}

	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return config, fmt.Errorf("failed to unmarshal yml file content: %w", err)
	}

	return config, nil
}
