package coreserver

import (
	"fmt"
	"sync"
	"sync/atomic"

	"quest_notifier_bot/internal/model"
	"quest_notifier_bot/internal/storage"
)

// Should be created with NewService(). All methods are safe for concurrent use.
type Server struct {
	storage storage.Storage

	// protected by mu
	notifiable UserNotifiable
	mu         sync.Mutex

	isShutdowned int32
	wg           sync.WaitGroup
}

func NewServer(s storage.Storage) (*Server, error) {
	return &Server{
		storage:      s,
		isShutdowned: 0,
	}, nil
}

func (s *Server) SetUserNotifiable(n UserNotifiable) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.notifiable = n
}

func (s *Server) Subscribe(user model.UserID, quest model.QuestID) error {
	exist, err := s.storage.Quest().Exists(quest)
	if err != nil {
		return err
	}

	if !exist {
		return model.BadRequestError{Message: "This quest not exists or not supported"}
	}

	err = s.storage.Subscription().Add(user, quest)
	if err != nil {
		return err
	}

	return nil
}

func (s *Server) Unsubscribe(user model.UserID, quest model.QuestID) error {
	return s.storage.Subscription().Remove(user, quest)
}

func (s *Server) GetSubscriptions(user model.UserID) ([]model.Quest, error) {
	questIDs, err := s.storage.Subscription().GetSubscriptions(user)
	if err != nil {
		return nil, err
	}

	names, err := s.storage.Quest().GetNames(questIDs)
	if err != nil {
		return nil, err
	}

	result := make([]model.Quest, len(questIDs))
	for i, questID := range questIDs {
		name, exists := names[questID]
		if !exists {
			return nil, fmt.Errorf("%w: cannot find name for valid quest id = %d", ErrServiceInternal, questID)
		}

		result[i] = model.Quest{
			ID:   questID,
			Name: name,
		}
	}

	return result, nil
}

func (s *Server) GetNotified(quests []model.Quest) chan error {
	ec := make(chan error, 1)
	if atomic.LoadInt32(&s.isShutdowned) != 0 {
		ec <- fmt.Errorf("shutdowned")
		close(ec)
		return ec
	}

	go s.handleUpdates(quests, ec)
	return ec
}

func (s *Server) handleUpdates(quests []model.Quest, ec chan error) {
	defer close(ec)

	questIDs := []model.QuestID{}
	questID2questIdx := map[model.QuestID]int{}
	for idx, quest := range quests {
		questIDs = append(questIDs, quest.ID)
		questID2questIdx[quest.ID] = idx
	}

	subscribers, err := s.storage.Subscription().GetSubscribers(questIDs)
	if err != nil {
		ec <- err
		return
	}

	updates := map[model.UserID][]model.Quest{}
	for questID, users := range subscribers {
		for _, userID := range users {
			questIdx := questID2questIdx[questID]
			updates[userID] = append(updates[userID], quests[questIdx])
		}
	}

	s.mu.Lock()
	defer s.mu.Unlock()
	if s.notifiable != nil {
		s.notifiable.GetNotified(updates)
	}

	ec <- nil
	return
}

// May block calling goroutine
func (s *Server) Shutdown() {
	atomic.StoreInt32(&s.isShutdowned, 1)

	s.wg.Wait()
}
