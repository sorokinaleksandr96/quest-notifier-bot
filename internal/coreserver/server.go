package coreserver

import (
	"errors"

	"quest_notifier_bot/internal/model"
)

var (
	ErrServiceInternal = errors.New("internal server error")
)

type UserNotifiable interface {
	GetNotified(map[model.UserID][]model.Quest)
}

type CoreServer interface {
	SetUserNotifiable(n UserNotifiable)
	Subscribe(user model.UserID, quest model.QuestID) error
	Unsubscribe(user model.UserID, quest model.QuestID) error
	GetSubscriptions(user model.UserID) ([]model.Quest, error)
	// Should be non-blocking
	GetNotified(quests []model.Quest) chan error
	Shutdown()
}
