package coreserver

import (
	"quest_notifier_bot/internal/model"

	"github.com/sirupsen/logrus"
)

type LoggingServer struct {
	impl   CoreServer
	logger logrus.FieldLogger
}

func NewLoggingServer(impl CoreServer, logger logrus.FieldLogger) *LoggingServer {
	return &LoggingServer{impl: impl, logger: logger}
}

func (s *LoggingServer) SetUserNotifiable(n UserNotifiable) {
	s.impl.SetUserNotifiable(n)
}

func (s *LoggingServer) Subscribe(user model.UserID, quest model.QuestID) error {
	logger := s.logger.WithFields(logrus.Fields{
		"action": "subscribe",
		"user":   user,
		"quest":  quest,
	})

	err := s.impl.Subscribe(user, quest)
	if err != nil {
		lr := logger.WithFields(logrus.Fields{
			"result": "error",
			"data":   err,
		})

		if _, casted := err.(model.BadRequestError); casted {
			lr.Warn()
		} else {
			lr.Error()
		}
		return err
	}

	logger.WithField("result", "success").Info()
	return nil
}

func (s *LoggingServer) Unsubscribe(user model.UserID, quest model.QuestID) error {
	logger := s.logger.WithFields(logrus.Fields{
		"action": "unsubscribe",
		"user":   user,
		"quest":  quest,
	})

	err := s.impl.Unsubscribe(user, quest)
	if err != nil {
		logger.WithFields(logrus.Fields{
			"result": "error",
			"data":   err,
		}).Error()
		return err
	}

	logger.WithField("result", "success").Info()
	return nil
}

func (s *LoggingServer) GetSubscriptions(user model.UserID) ([]model.Quest, error) {
	logger := s.logger.WithFields(logrus.Fields{
		"action": "get_subscriptions",
		"user":   user,
	})

	subs, err := s.impl.GetSubscriptions(user)
	if err != nil {
		logger.WithFields(logrus.Fields{
			"result": "error",
			"data":   err,
		}).Error()
		return nil, err
	}

	logger.WithFields(logrus.Fields{
		"result": "success",
		"data":   subs,
	}).Info()
	return subs, nil
}

func (s *LoggingServer) GetNotified(quests []model.Quest) chan error {
	ch := make(chan error, 1)

	go func() {
		defer close(ch)
		logger := s.logger.WithFields(logrus.Fields{
			"action":     "got_notified",
			"num_quests": len(quests),
		})

		err := <-s.impl.GetNotified(quests)
		if err != nil {
			logger.WithFields(logrus.Fields{
				"result": "error",
				"data":   err,
			}).Error()
		} else {
			logger.WithField("result", "success").Info()
		}

		ch <- err
	}()

	return ch
}

func (s *LoggingServer) Shutdown() {
	s.impl.Shutdown()
	s.logger.Info("terminated")
}
