package storage

import "quest_notifier_bot/internal/model"

type Storage interface {
	Subscription() SubscriptionRepository
	Quest() QuestRepository
}

type QuestRepository interface {
	Exists(model.QuestID) (bool, error)
	Update([]model.Quest) error
	GetNames(ids []model.QuestID) (map[model.QuestID]string, error)
}

type SubscriptionRepository interface {
	Add(model.UserID, model.QuestID) error
	Remove(model.UserID, model.QuestID) error
	RemoveAll(model.UserID) error
	GetSubscriptions(model.UserID) ([]model.QuestID, error)
	GetSubscribers([]model.QuestID) (map[model.QuestID][]model.UserID, error)
}
