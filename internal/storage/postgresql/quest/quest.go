package quest

import (
	"database/sql"
	"fmt"
	"strings"

	"quest_notifier_bot/internal/model"
)

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) (*Repository, error) {
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS
			quest (
				quest_id	INT,
				quest_name	TEXT,
				PRIMARY KEY (quest_id)
			)
	`)

	if err != nil {
		return nil, err
	}

	return &Repository{db: db}, nil
}

func (r *Repository) Update(us []model.Quest) error {
	if len(us) == 0 {
		return nil
	}

	sb := strings.Builder{}
	sb.WriteString(`
		INSERT INTO
			quest
		VALUES
			($1, $2)`)

	args := []interface{}{us[0].ID, us[0].Name}
	for i, q := range us[1:] {
		sb.WriteString(fmt.Sprintf(`
			,($%d, $%d)`, 2*(i+1)+1, 2*(i+2)))
		args = append(args, q.ID, q.Name)
	}
	sb.WriteString(`
		ON CONFLICT (quest_id) DO UPDATE
		SET quest_name = EXCLUDED.quest_name`)

	_, err := r.db.Query(sb.String(), args...)
	return err
}

func (r *Repository) GetNames(ids []model.QuestID) (map[model.QuestID]string, error) {
	rows, err := r.db.Query(`
		SELECT
			quest_id,
			quest_name
		FROM
			quest
		WHERE
			quest_id = ANY($1)
	`, ids)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	m := map[model.QuestID]string{}

	tmp := model.Quest{}
	for rows.Next() {
		err = rows.Scan(&tmp.ID, &tmp.Name)
		if err != nil {
			return nil, err
		}
		m[tmp.ID] = tmp.Name
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return m, nil
}

func (r *Repository) Exists(id model.QuestID) (bool, error) {
	var res bool
	err := r.db.QueryRow(`
	SELECT 
		EXISTS(
			SELECT 
				1 
			FROM 
				quest 
			WHERE
				quest_id = $1
		)
	`, id).Scan(&res)

	return res, err
}
