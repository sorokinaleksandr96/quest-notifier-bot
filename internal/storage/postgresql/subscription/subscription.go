package subscription

import (
	"database/sql"
	"quest_notifier_bot/internal/model"
)

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) (*Repository, error) {
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS
			subscription (
				user_id 	BIGINT,
				quest_id	INT,
				PRIMARY KEY (user_id, quest_id)
			)
	`)

	if err != nil {
		return nil, err
	}

	return &Repository{db: db}, nil
}

func (r *Repository) Add(user model.UserID, quest model.QuestID) error {
	_, err := r.db.Exec(`
		INSERT INTO
			subscription
		VALUES
			($1, $2)
		ON CONFLICT DO NOTHING
	`, user, quest,
	)

	return err
}

func (r *Repository) Remove(user model.UserID, quest model.QuestID) error {
	_, err := r.db.Exec(`
		DELETE FROM
			subscription
		WHERE
			user_id = $1
			and quest_id = $2
	`, user, quest,
	)

	return err
}

func (r *Repository) RemoveAll(user model.UserID) error {
	_, err := r.db.Exec(`
		DELETE FROM
			subscription
		WHERE
			user_id = $1
	`, user,
	)

	return err
}

func (r *Repository) GetSubscriptions(user model.UserID) ([]model.QuestID, error) {
	rows, err := r.db.Query(`
		SELECT
			quest_id
		FROM
			subscription
		WHERE
			user_id = $1
	`, user,
	)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := []model.QuestID{}
	var questID model.QuestID

	for rows.Next() {
		err = rows.Scan(&questID)
		if err != nil {
			return nil, err
		}
		result = append(result, questID)
	}

	return result, rows.Err()
}

func (r *Repository) GetSubscribers(quests []model.QuestID) (map[model.QuestID][]model.UserID, error) {
	rows, err := r.db.Query(`
		SELECT
			user_id,
			quest_id
		FROM
			subscription
		WHERE
			quest_id = ANY($1)
	`, quests,
	)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := map[model.QuestID][]model.UserID{}
	var userID model.UserID
	var questID model.QuestID

	for rows.Next() {
		err = rows.Scan(&userID, &questID)
		if err != nil {
			return nil, err
		}
		result[questID] = append(result[questID], userID)
	}

	return result, rows.Err()
}
