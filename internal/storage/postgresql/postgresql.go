package postgresql

import (
	"database/sql"
	"fmt"

	_ "github.com/jackc/pgx/v4/stdlib"

	"quest_notifier_bot/internal/storage"
	"quest_notifier_bot/internal/storage/postgresql/quest"
	"quest_notifier_bot/internal/storage/postgresql/subscription"
)

type Config struct {
	User     string
	Password string
	Host     string
	Port     string
	DB       string
}

type StorageImpl struct {
	db *sql.DB

	q *quest.Repository
	s *subscription.Repository
}

func NewStorageFromConfig(c Config) (*StorageImpl, error) {
	return NewStorage(c.User, c.Password, c.Host, c.Port, c.DB)
}

func NewStorage(
	user string, password string,
	host string, port string, db string,
) (*StorageImpl, error) {
	connStr := fmt.Sprintf("postgres://%s:%s@%s:%s/%s", user, password, host, port, db)

	dbh, err := sql.Open("pgx", connStr)
	if err != nil {
		return nil, err
	}

	err = dbh.Ping()
	if err != nil {
		dbh.Close()
		return nil, err
	}

	q, err := quest.NewRepository(dbh)
	if err != nil {
		dbh.Close()
		return nil, fmt.Errorf("cannot create quest repo: %w", err)
	}

	s, err := subscription.NewRepository(dbh)
	if err != nil {
		dbh.Close()
		return nil, fmt.Errorf("cannot create subscription repo: %w", err)
	}

	return &StorageImpl{
		db: dbh,

		q: q,
		s: s,
	}, nil
}

func (s *StorageImpl) Close() {
	s.db.Close()
}

func (s *StorageImpl) Quest() storage.QuestRepository {
	return s.q
}

func (s *StorageImpl) Subscription() storage.SubscriptionRepository {
	return s.s
}
