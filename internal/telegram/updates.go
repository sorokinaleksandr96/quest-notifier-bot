package telegram

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"quest_notifier_bot/internal/model"
)

func generateMessage(updates []model.Quest) string {
	if len(updates) < 1 {
		return ""
	}

	sorted := makeQuestsSorted(updates)
	return questsToMessage(sorted, time.Now())
}

func makeQuestsSorted(quests []model.Quest) [][]model.Quest {
	ending2idx := map[time.Time]int{}
	grouped := [][]model.Quest{}

	for _, q := range quests {
		endingIdx, exists := ending2idx[q.Ending]
		if !exists {
			ending2idx[q.Ending] = len(ending2idx)
			grouped = append(grouped, nil)
		}

		grouped[endingIdx] = append(grouped[endingIdx], q)
	}

	sort.Slice(grouped, func(i, j int) bool {
		return grouped[i][0].Ending.Before(grouped[j][0].Ending)
	})

	for idx := range grouped {
		sort.Slice(grouped[idx], func(i, j int) bool {
			return grouped[idx][i].ID < grouped[idx][j].ID
		})
	}

	return grouped
}

// expected already grouped by ending time and sorted quests
func questsToMessage(quests [][]model.Quest, moment time.Time) string {
	sb := strings.Builder{}

	sb.WriteString("The following quests have appeared:\n")

	for _, subquests := range quests {
		timeToExpire := subquests[0].Ending.Sub(moment)
		if timeToExpire < 0 {
			continue
		}

		sb.WriteString("\n")
		timeToExpireStr := durationToText(timeToExpire)
		sb.WriteString("Expiring in " + timeToExpireStr + "\n")
		for i, quest := range subquests {
			sb.WriteString(fmt.Sprintf("%d. [%s](https://www.wowhead.com/quest=%d) (%d)\n", i+1, quest.Name, quest.ID, quest.ID))
		}
	}

	return sb.String()
}

func durationToText(dur time.Duration) string {
	switch {
	case dur < time.Hour:
		return "less than an hour"
	case dur < 2*time.Hour:
		return "an hour"
	case dur < 24*time.Hour:
		return fmt.Sprintf("%d hours", dur/time.Hour)
	}

	return "more than a day"
}
