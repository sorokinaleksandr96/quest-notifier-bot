package telegram

import (
	"fmt"
	"sort"
	"strings"

	tele "gopkg.in/telebot.v3"

	"quest_notifier_bot/internal/model"
)

var (
	internalErrorString = "internal error, sorry :("
)

func trashHandler() tele.HandlerFunc {
	return func(ctx tele.Context) error {
		isCommand := false
		for _, r := range ctx.Text() {
			isCommand = (r == '/')
			break
		}

		var firstPart string
		if isCommand {
			firstPart = "Unknown command. "
		} else {
			firstPart = "Expected command. "
		}

		ctx.Set("response_message", firstPart+"Type /help to get list of all supported commands")
		return nil
	}
}

func helpHandler() tele.HandlerFunc {
	return func(ctx tele.Context) error {
		ctx.Set(
			"response_message",

			"This bot is designed to track wow quests and notificate when they appearing.\n\n"+
				"Supported commands:\n"+
				"1. /help — Recive this message again.\n"+
				"2. /sub <quest_id> — Start tracking quest. When it become live, this bot will send you a notification.\n"+
				"3. /unsub <quest_id> — Stop tracking quest. You will no logner recive notifications about it.\n"+
				"4. /list — Recive list of all your quests you are tracking so far.",
		)
		return nil
	}
}

func subscribeHandler(s CoreServer) tele.HandlerFunc {
	return func(ctx tele.Context) error {
		ctx.Set("action", "/sub")

		quest_id, casted := ctx.Get("quest_id").(model.QuestID)
		if !casted {
			ctx.Set("response_message", internalErrorString)
			ctx.Set("result", "error")
			ctx.Set("error", fmt.Errorf("quest_id missed in ctx"))
			return nil
		}
		ctx.Set("arg", quest_id)

		user := model.UserID(ctx.Chat().ID)
		if err := s.Subscribe(user, quest_id); err != nil {
			if brerr, ok := err.(model.BadRequestError); ok {
				ctx.Set("response_message", brerr.Message)
				ctx.Set("result", "bad_request")
			} else {
				ctx.Set("response_message", internalErrorString)
				ctx.Set("result", "error")
				ctx.Set("error", err)
			}
			return nil
		}

		ctx.Set("response_message", "successfully subscribed")
		ctx.Set("result", "success")
		return nil
	}
}

func unsubscribeHandler(s CoreServer) tele.HandlerFunc {
	return func(ctx tele.Context) error {
		ctx.Set("action", "/unsub")

		quest_id, casted := ctx.Get("quest_id").(model.QuestID)
		if !casted {
			ctx.Set("response_message", internalErrorString)
			ctx.Set("result", "error")
			ctx.Set("error", fmt.Errorf("quest_id missed in ctx"))
			return nil
		}
		ctx.Set("arg", quest_id)

		user := model.UserID(ctx.Chat().ID)
		if err := s.Unsubscribe(user, quest_id); err != nil {
			ctx.Set("response_message", internalErrorString)
			ctx.Set("result", "error")
			ctx.Set("error", err)
			return nil
		}

		ctx.Set("response_message", "successfully unsubscribed")
		ctx.Set("result", "success")
		return nil
	}
}

func subscriptionsHandler(s CoreServer) tele.HandlerFunc {
	return func(ctx tele.Context) error {
		ctx.Set("action", "/list")

		user := model.UserID(ctx.Chat().ID)
		list, err := s.GetSubscriptions(user)
		if err != nil {
			ctx.Set("response_message", internalErrorString)
			ctx.Set("result", "error")
			ctx.Set("error", err)
			return nil
		}

		sort.Slice(list, func(i, j int) bool {
			return list[i].ID < list[j].ID
		})

		sb := strings.Builder{}
		if len(list) > 0 {
			sb.WriteString("You are subscribed to:\n")
			for i, quest := range list {
				sb.WriteString(fmt.Sprintf("%d. [%s](https://www.wowhead.com/quest=%d) (%d)\n", i+1, quest.Name, quest.ID, quest.ID))
			}
		} else {
			sb.WriteString("You have no subscriptions")
		}

		ctx.Set("response_message", sb.String())
		ctx.Set("result", "success")
		return nil
	}
}
