package telegram

import (
	"context"
	"fmt"
	"sync"
	"time"

	"quest_notifier_bot/internal/model"

	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
	tele "gopkg.in/telebot.v3"
)

var (
	log = logrus.WithField("service", "telegram")
)

type Config struct {
	Token         string
	PollerTimeout time.Duration // 10 sec default
	MsgSendLimit  int           // 30 msg/s default
}

type Server struct {
	bot        *tele.Bot
	limiter    *rate.Limiter
	coreserver CoreServer

	// For outcoming communications
	wg sync.WaitGroup
}

func NewServer(cs CoreServer, cfg Config) (*Server, error) {
	if cs == nil {
		return nil, fmt.Errorf("CoreServer is nil")
	}

	if cfg.PollerTimeout == 0 {
		cfg.PollerTimeout = 10 * time.Second
	}
	bot, err := tele.NewBot(tele.Settings{
		Token:     cfg.Token,
		Poller:    &tele.LongPoller{Timeout: cfg.PollerTimeout},
		ParseMode: tele.ModeMarkdown,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to create tele.bot instance: %w", err)
	}

	if cfg.MsgSendLimit == 0 {
		cfg.MsgSendLimit = 30
	}
	limiter := rate.NewLimiter(rate.Limit(cfg.MsgSendLimit), cfg.MsgSendLimit)

	s := &Server{
		bot:        bot,
		limiter:    limiter,
		coreserver: cs,
	}
	s.init()
	return s, nil
}

func (s *Server) init() {
	loggingMiddleware := getLoggingMiddleware(log)
	sendMessageMiddleware := getSendMessageMiddleware(s.limiter)

	s.bot.Use(sendMessageMiddleware, loggingMiddleware)

	s.bot.Handle("/help", helpHandler())
	s.bot.Handle("/start", helpHandler())
	s.bot.Handle("/sub", subscribeHandler(s.coreserver), questIDInputMiddleware)
	s.bot.Handle("/unsub", unsubscribeHandler(s.coreserver), questIDInputMiddleware)
	s.bot.Handle("/list", subscriptionsHandler(s.coreserver))
	s.bot.Handle(tele.OnText, trashHandler())
}

func (s *Server) GetNotified(updates map[model.UserID][]model.Quest) {
	s.wg.Add(1)
	go func() {
		defer s.wg.Done()
		s._getNotified(updates)
	}()
}

func (s *Server) _getNotified(updates map[model.UserID][]model.Quest) {
	for user, updates := range updates {
		s.wg.Add(1)
		go func(user model.UserID, updates []model.Quest) {
			defer s.wg.Done()
			s.sendUpdate(user, updates)
		}(user, updates)
	}
}

func (s *Server) sendUpdate(user model.UserID, updates []model.Quest) {
	if len(updates) < 1 {
		return
	}

	message := generateMessage(updates)
	s.limiter.Wait(context.Background())
	s.bot.Send(&tele.Chat{ID: int64(user)}, message, tele.NoPreview)
	log.WithFields(logrus.Fields{
		"action": "notify",
		"user":   user,
	}).Info()
}

func (s *Server) Run() {
	s.bot.Start()
}

func (s *Server) Shutdown() {
	s.bot.Stop()
	s.wg.Wait()
	log.Info("terminated")
}
