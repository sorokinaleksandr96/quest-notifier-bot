package telegram

import "quest_notifier_bot/internal/model"

type CoreServer interface {
	Subscribe(user model.UserID, quest model.QuestID) error
	Unsubscribe(user model.UserID, quest model.QuestID) error
	GetSubscriptions(user model.UserID) ([]model.Quest, error)
}
