package telegram

import (
	"context"
	"quest_notifier_bot/internal/model"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
	tele "gopkg.in/telebot.v3"
)

func getSendMessageMiddleware(l *rate.Limiter) tele.MiddlewareFunc {
	return func(hf tele.HandlerFunc) tele.HandlerFunc {
		return func(ctx tele.Context) error {
			err := hf(ctx)
			if err != nil {
				return err
			}

			message := ctx.Get("response_message")
			if message == "" {
				return nil
			}

			l.Wait(context.Background())
			return ctx.Send(message, tele.NoPreview)
		}
	}
}

// Should be the last middleware, because it interuptipts the chaining
// TODO: Maybe should always call next, but then callee should test,
// for example, that "response_message" is not set
func questIDInputMiddleware(hf tele.HandlerFunc) tele.HandlerFunc {
	return func(ctx tele.Context) error {
		if len(ctx.Args()) != 1 {
			if len(ctx.Args()) == 0 {
				ctx.Set("response_message", "expected numeric id of quest")
				return nil
			}
			ctx.Set("response_message", "expected only one argument: quest id")
			return nil
		}

		quest_id, err := strconv.Atoi(ctx.Message().Payload)
		if err != nil {
			ctx.Set("response_message", "expected numeric id of quest")
			return nil
		}

		ctx.Set("quest_id", model.QuestID(quest_id))
		return hf(ctx)
	}
}

var (
	possibleFields = []string{
		"action",
		"arg",
		"result",
		"error",
	}
)

func getLoggingMiddleware(logger logrus.FieldLogger) tele.MiddlewareFunc {
	return func(hf tele.HandlerFunc) tele.HandlerFunc {
		return func(ctx tele.Context) error {
			start := time.Now()
			err := hf(ctx)

			fields := logrus.Fields{}
			for _, field := range possibleFields {
				value := ctx.Get(field)
				if value != nil {
					fields[field] = value
				}
			}

			fields["user"] = ctx.Chat().ID
			fields["elapsed"] = time.Since(start)

			if ctx.Get("action") != nil {
				if ctx.Get("error") != nil {
					logger.WithFields(fields).Error()
				} else {
					logger.WithFields(fields).Info()
				}
			} else {
				logger.WithFields(fields).Trace()
			}
			return err
		}
	}
}
