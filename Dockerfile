FROM golang:bullseye

# Because we can build only in gopath
WORKDIR /go/questnotifierbot/

COPY go.mod go.sum ./
RUN go mod download

COPY internal internal
COPY pkg pkg
COPY cmd cmd

RUN go build -o app cmd/main.go

CMD ["/go/questnotifierbot/app"]